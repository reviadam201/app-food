import foodData from "../../utilities/foods.json";

export const foodActionTypes = {
  GET_FOOD: "GET_FOOD",
  ADD_FOOD: "ADD_FOOD",
  READ_FOOD: "READ_FOOD",
};

export const getFood = () => (dispatch) => {
  return dispatch({ type: foodActionTypes.GET_FOOD, data: foodData });
};

export const addFood = (payload) => (dispatch) => {
  return dispatch({ type: foodActionTypes.ADD_FOOD, payload });
};
