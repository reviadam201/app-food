import { foodActionTypes } from "./action";

const foodInitialState = {
  foods: null,
};

export default function reducer(state = foodInitialState, action) {
  switch (action.type) {
    case foodActionTypes.GET_FOOD:
      return Object.assign({}, state, {
        foods: action.data,
      });
      break;
    case foodActionTypes.ADD_FOOD:
      return Object.assign({}, state, {
        foods: [...state.foods, action.payload],
      });
      break;
    default:
      return state;
  }
}
