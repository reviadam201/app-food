import registerStyle from "./register.module.css";
import React, { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col"

export default function RegisterPage() {
  const router = useRouter();
  const [username, isiUsername] = useState("");
  const [password, isiPassword] = useState("");
  const [email, isiEmail] = useState("");
  const [phone, isiPhone] = useState("");

  const mengarahkanKeLogin = () => {
    router.push("/login");
  };

  const submit = () => {
    var payload = {
      username: username,
      password: password,
      email: email,
      mobilePhoneNumber: phone,
    };
    axios
      .post("https://simple-crud-by-efge.herokuapp.com/users", payload)
      .then((response) => {
        sessionStorage.setItem(
          "sudah-login",
          JSON.stringify(response.data.data)
        );
        window.location.reload();
      });

    console.log(payload);
  };

  return (
    <>
      <div>
        <div className={registerStyle.container}>
          <div className={registerStyle.headerForm}>
            <span className={registerStyle.headerFormTitle}>REGISTER</span>
          </div>
          <div className={registerStyle.bodyForm}>
            <div className={registerStyle.bodyFormAttendee}>
              <div>
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Username*</Form.Label>
                    <Form.Control onChange={(e) => isiUsername(e.target.value)} type="email" placeholder="Username" />
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridPassword">
                    <Form.Label>Password*</Form.Label>
                    <Form.Control onChange={(e) => isiPassword(e.target.value)} type="password" placeholder="Password" />
                  </Form.Group>
                </Row>
              </div>
              <div>
              <Row className="mb-3">
                  <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Email*</Form.Label>
                    <Form.Control onChange={(e) => isiEmail(e.target.value)} type="email" placeholder="Enter email" />
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridPassword">
                    <Form.Label>Phone</Form.Label>
                    <Form.Control onChange={(e) => isiPhone(e.target.value)} type="Phone" placeholder="Phone" />
                  </Form.Group>
                </Row>
              </div>
              <div className={registerStyle.formButton1}>
                <Button onClick={submit} variant="secondary" type="submit">
                  REGISTER
                </Button>
              </div>
              <div className={registerStyle.formButton1}>
                <Button onClick={mengarahkanKeLogin} variant="link" type="submit">
                  Sudah punya akun
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

// npm run dev
