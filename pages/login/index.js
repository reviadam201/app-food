import loginStyle from "./login.module.css";
import React, { useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export default function LoginPage() {
  const router = useRouter();
  const [email, isiEmail] = useState("");
  const [password, isiPassword] = useState("");

  const mengarahkanKeRegitser = () => {
    router.push("/register");
  };

  const login = () => {
    var payload = {
      email: email,
      password: password,
    };
    axios
      .post("https://simple-crud-by-efge.herokuapp.com/users/login", payload)
      .then((response) => {
        sessionStorage.setItem(
          "sudah-login",
          JSON.stringify(response.data.data)
        );
        window.location.reload();
      });

    console.log(payload);
  };

  return (
    <>
      <div>
        <div className={loginStyle.container}>
          <div className={loginStyle.headerForm}>
            <span className={loginStyle.headerFormTitle}>LOGIN</span>
          </div>
          <div className={loginStyle.bodyForm}>
            <div className={loginStyle.bodyFormAttendee}>
              <div>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email*</Form.Label>
                  <Form.Control onChange={(e) => isiEmail(e.target.value)} type="email" placeholder="Enter email" />
                  <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                  </Form.Text>
                </Form.Group>
              </div>
              <div>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Password*</Form.Label>
                  <Form.Control onChange={(e) => isiPassword(e.target.value)} type="password" placeholder="Password" />
                </Form.Group>
              </div>
              <div>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                  <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
              </div>
              <div className={loginStyle.formButton}>
                <Button onClick={login} variant="secondary" type="submit">
                  LOGIN
                </Button>
              </div>
              <div className={loginStyle.formButton}>
                <Button onClick={mengarahkanKeRegitser} variant="link" type="submit">
                  Belum punya akun
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
