import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getFood, addFood } from "../store/foods/action";
import { wrapper } from "../store/store";

const Index = (props) => {
  const [foodStates, setFoodStates] = useState([]);
  const [nameTxt, setNameTxt] = useState("");
  const [priceTxt, setPriceTxt] = useState("");
  const [restaurantTxt, setRestauranTxt] = useState("");
  const submit = () => {
    props.addFood({
      name: nameTxt,
      price: priceTxt,
      sold: 0,
      restaurant: restaurantTxt,
    });
  };
  const gettingFood = () => {};
  useEffect(() => {
    props.getFood();
    setFoodStates(props.foodProps.foods);
  }, []);
  return (
    <div>
      <input
        placeholder="name"
        onChange={(event) => setNameTxt(event.target.value)}
      />
      <br />
      <br />
      <input
        placeholder="price"
        onChange={(event) => setPriceTxt(event.target.value)}
      />
      <br />
      <br />
      <input
        placeholder="restoran"
        onChange={(event) => setRestauranTxt(event.target.value)}
      />
      <br />
      <br />
      <button onClick={submit}>Submit</button>
      <br />
      <br />
      {props.foodProps.foods.map((index) => {
        return (
          <div>
            <span>{index.name}</span>
            <br />
          </div>
        );
      })}
    </div>
  );
};

export const getStaticProps = wrapper.getStaticProps((store) => () => {
  store.dispatch(getFood());
});

const mapStateToProps = (state) => ({
  foodProps: state.foods,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getFood: bindActionCreators(getFood, dispatch),
    addFood: bindActionCreators(addFood, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
